# Uxig - Timer App for Fitness Boxing Training

![Uxig logo](Uxig-logo-identicon.png)

## Source
https://bitbucket.org/truedrcb/uxig/

## Latest version � 1.1
[Download Uxig.jar](https://bitbucket.org/truedrcb/uxig/downloads/Uxig.jar)

## Features
* Normal (time) mode
  * Current time (24h format) with seconds
  * Permanent progress bar: 1 minute based on current time seconds
* Timer buttons: 1, 2 and 3 minutes for short training intervals
* Timer mode
  * Progress bar based on selected interval
  * Count down timer
  * Stop button
* Sound for interval start and end

## Screenshots