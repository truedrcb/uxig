package com.gratchev.uxig;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.SwingPropertyChangeSupport;

import com.google.fonts.OpenFontsDummy;

public class Uxig {

	private volatile int startMinutes = 0; 

	// https://stackoverflow.com/a/17861016/1099503
	public static byte[] readAllBytes(InputStream is) throws IOException
	{
		try (final ByteArrayOutputStream os = new ByteArrayOutputStream();)
		{
			final byte[] buffer = new byte[0xFFFF];

			for (int len; (len = is.read(buffer)) != -1;)
				os.write(buffer, 0, len);

			os.flush();

			return os.toByteArray();
		}
	}

	abstract class ModelBase {
		protected final PropertyChangeEvent timeChangedEvent = new PropertyChangeEvent(this, "time", null, null);

		final SwingPropertyChangeSupport firer = new SwingPropertyChangeSupport(this);

		final DecimalFormat format = new DecimalFormat("00");
	}

	class TimeModel extends ModelBase {
		private final GregorianCalendar calendar = new GregorianCalendar();
		int ss = -1;
		String labelText = "--:--:--";

		void setMillis(long currentTimeMillis) {
			calendar.setTimeInMillis(currentTimeMillis);
			int ss = calendar.get(Calendar.SECOND);
			if (ss != this.ss) {
				int hh, mm;
				hh = calendar.get(Calendar.HOUR_OF_DAY);
				mm = calendar.get(Calendar.MINUTE);
				this.ss = ss;
				labelText = format.format(hh) + ":" + format.format(mm) + "." + format.format(ss);

				firer.firePropertyChange(timeChangedEvent);
			}
		}
	}

	class TimerModel extends ModelBase {
		final SwingPropertyChangeSupport switchFirer = new SwingPropertyChangeSupport(this);

		long timerEndMillis = 0L;
		int secondsLeft = -1;
		int secondsTotal = 1;
		String labelText = "--:--";

		void setMillis(long currentTimeMillis) {
			final int secondsLeft = (int)((timerEndMillis - currentTimeMillis - 1) / 1000L);
			if (secondsLeft != this.secondsLeft) {
				this.secondsLeft = secondsLeft;
				int s = secondsLeft % 60;
				int m = secondsLeft / 60;
				labelText = format.format(m) + ":" + format.format(s);

				firer.firePropertyChange(timeChangedEvent);
			}
		}

		void setStartMinutes(int startMinutes, long currentTimeMillis) {
			secondsTotal = startMinutes * 60;
			timerEndMillis = currentTimeMillis + 1000L * secondsTotal;
			switchFirer.firePropertyChange(timeChangedEvent);
		}
	}

	private final TimeModel timeModel = new TimeModel();
	private final TimerModel timerModel = new TimerModel();
	private Color background2;
	private Color foreground2;
	private Font bigFont;

	class AudioClip {
		final Clip clip;
		final ByteArrayInputStream inMemoryStream;

		public AudioClip() throws LineUnavailableException, UnsupportedAudioFileException, IOException, URISyntaxException {
			clip = AudioSystem.getClip();

			final byte[] buffer = readAllBytes(Uxig.class.getResourceAsStream("66951__benboncan__boxing-bell.wav"));
			inMemoryStream = new ByteArrayInputStream(buffer);
			final AudioInputStream inputStream = AudioSystem.getAudioInputStream(inMemoryStream);
			clip.open(inputStream);
		}

		private void playFromPosition(int offset) {
			clip.stop();
			clip.flush();
			clip.setFramePosition(offset);
			clip.start();
		}

		public void playStartSound() {
			playFromPosition(0);
		}

		public void playEndSound() {
			playFromPosition(13000);
		}
	}

	@SuppressWarnings("serial")
	class ArcLabel extends JLabel {
		private int arcStart = 90, arc = 120;

		public int getArcStart() {
			return arcStart;
		}

		public void setArcStart(int arcStart) {
			this.arcStart = arcStart;
		}

		public int getArc() {
			return arc;
		}

		public void setArc(int arc) {
			this.arc = arc;
		}

		@Override
		public void paint(Graphics graphics) {
			graphics.setColor(background2);
			graphics.fillArc(10, 10, getWidth() - 20, getHeight() - 20, arcStart, arc);
			graphics.setColor(foreground2);
			graphics.drawArc(10, 10, getWidth() - 20, getHeight() - 20, arcStart, arc);
			super.paint(graphics);
		}

	}

	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event-dispatching thread.
	 * @throws IOException 
	 * @throws FontFormatException 
	 * @throws LineUnavailableException 
	 * @throws UnsupportedAudioFileException 
	 * @throws URISyntaxException 
	 */
	private void createAndShowGUI() throws FontFormatException, IOException, LineUnavailableException, UnsupportedAudioFileException, URISyntaxException {

		final InputStream is = OpenFontsDummy.class.getResourceAsStream("ShareTechMono-Regular.ttf");
		final Font font = Font.createFont(Font.TRUETYPE_FONT, is);
		bigFont = font.deriveFont(200f);
		final Font buttonFont = font.deriveFont(120f);
		final Font tinyFont = font.deriveFont(20f);
		final Color background = Color.black;
		background2 = new Color(0, 50, 50);
		final Color foreground = Color.cyan;
		foreground2 = new Color(0, 150, 150);

		final Color tbackground = Color.black;
		final Color tbackground2 = new Color(50, 10, 10);
		final Color tforeground = new Color(255, 50, 50);;
		final Color tforeground2 = new Color(150, 30, 30);

		//Prepare sound clip
		final AudioClip bell = new AudioClip();

		//Create and set up the window.
		final JFrame frame = new JFrame("Uxig");
		frame.setIconImage(new ImageIcon(Uxig.class.getResource("Uxig-logo-identicon.png")).getImage());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final Container contentPane = frame.getContentPane();
		contentPane.setLayout(new BorderLayout());
		contentPane.setBackground(background);


		final JPanel timePanel = new JPanel(new BorderLayout());
		timePanel.setBackground(tbackground);

		final ArcLabel timeLabel = new ArcLabel();
		timeLabel.setText("--:--:--");
		timeLabel.setArc(10);
		timeLabel.setFont(bigFont);
		timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		timeLabel.setVerticalAlignment(SwingConstants.TOP);
		timeLabel.setBorder(BorderFactory.createEmptyBorder(50, 50, 50, 50));
		timeLabel.setForeground(foreground);
		timePanel.add(timeLabel, BorderLayout.CENTER);



		timeModel.firer.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				timeLabel.setText(timeModel.labelText);
				timeLabel.setArcStart((360 - timeModel.ss * 6) + 90);
			}
		});






		final JPanel timerPanel = new JPanel(new BorderLayout());
		timerPanel.setBackground(tbackground);

		final JPanel timerTopPanel = new JPanel(new BorderLayout());
		timerTopPanel.setBackground(tbackground);
		timerPanel.add(timerTopPanel, BorderLayout.NORTH);

		final JProgressBar timerProgressBar = new JProgressBar(0, 300);
		{
			final JPanel progressPanel = new JPanel(new BorderLayout());
			timerProgressBar.setForeground(tforeground);
			timerProgressBar.setBackground(tbackground2);
			timerProgressBar.setBorder(BorderFactory.createEmptyBorder());
			timerProgressBar.setBorderPainted(false);
			progressPanel.add(timerProgressBar, BorderLayout.CENTER);
			progressPanel.setBackground(background);
			final JLabel p1 = new JLabel(":");
			p1.setForeground(tforeground2);
			p1.setFont(tinyFont);
			p1.setHorizontalAlignment(SwingConstants.CENTER);
			progressPanel.add(p1, BorderLayout.NORTH);
			final JLabel p2 = new JLabel(":");
			p2.setForeground(tforeground2);
			p2.setFont(tinyFont);
			p2.setHorizontalAlignment(SwingConstants.CENTER);
			progressPanel.add(p2, BorderLayout.SOUTH);
			timerProgressBar.setPreferredSize(new Dimension(50, 50));
			timerTopPanel.add(progressPanel, BorderLayout.NORTH);
		}

		final JButton stopButton = new JButton("Stop");
		stopButton.setBorderPainted(false);
		stopButton.setFont(buttonFont);
		stopButton.setBackground(tbackground);
		stopButton.setForeground(tforeground2);
		timerTopPanel.add(stopButton, BorderLayout.CENTER);

		final JLabel timerLabel = new JLabel("--:--");
		timerLabel.setFont(bigFont);
		timerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		timerLabel.setVerticalAlignment(SwingConstants.CENTER);
		timerLabel.setBorder(BorderFactory.createEmptyBorder(50, 50, 50, 50));
		timerLabel.setForeground(tforeground);
		timerPanel.add(timerLabel, BorderLayout.CENTER);

		stopButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				startMinutes = -1;
			}
		});

		timerModel.switchFirer.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				timerProgressBar.setMinimum(0);
				timerProgressBar.setMaximum(timerModel.secondsTotal);
				timerProgressBar.setValue(0);
			}
		});

		timerModel.firer.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				timerLabel.setText(timerModel.labelText);
				timerProgressBar.setValue(timerModel.secondsTotal - timerModel.secondsLeft);
			}
		});




		final CardLayout timeLayout = new CardLayout();
		final JPanel timeTimerPanel = new JPanel(timeLayout);
		timeTimerPanel.setBackground(background);
		contentPane.add(timeTimerPanel, BorderLayout.CENTER);

		timeTimerPanel.add(timePanel, "time");
		timeTimerPanel.add(timerPanel, "timer");
		timeLayout.show(timeTimerPanel, "time");



		final JPanel buttonsPanel = new JPanel(new GridLayout(1, 2, 5, 5));
		buttonsPanel.setBackground(background2);
		contentPane.add(buttonsPanel, BorderLayout.SOUTH);

		for(int i = 0; i < 3; i++) {
			final int minutes = i + 1;
			final JButton b = new JButton("" + minutes + " Min");
			b.setBorderPainted(false);
			b.setFont(buttonFont);
			b.setBackground(background);
			b.setForeground(foreground2);
			buttonsPanel.add(b);

			b.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					startMinutes = minutes;
				}
			});
		}



		final Timer timer = new Timer(250, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				long currentTimeMillis = System.currentTimeMillis();

				if (startMinutes > 0) {
					bell.playStartSound();
					timerModel.setStartMinutes(startMinutes, currentTimeMillis);
					startMinutes = 0;
					timeLayout.show(timeTimerPanel, "timer");
				}

				if (timerModel.timerEndMillis > 0) {
					if (startMinutes < 0 || currentTimeMillis > timerModel.timerEndMillis) {
						if (currentTimeMillis > timerModel.timerEndMillis) {
							bell.playEndSound();
						}
						timerModel.timerEndMillis = 0;
						timeLayout.show(timeTimerPanel, "time");
					}
				}

				if (timerModel.timerEndMillis > 0) {
					timerModel.setMillis(currentTimeMillis);
				} else {
					timeModel.setMillis(currentTimeMillis);
				}
			}
		});

		timer.setInitialDelay(100);
		timer.start();

		//Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		//Schedule a job for the event-dispatching thread:
		//creating and showing this application's GUI.
		final Uxig uxig = new Uxig();

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					uxig.createAndShowGUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
